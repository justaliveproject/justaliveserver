package sfs2x.extensions.games.justalive;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import just.alive.server.GameExtension;
import sfs2x.extensions.games.justalive.core.World;
import sfs2x.extensions.games.justalive.entities.Item;
import sfs2x.extensions.games.justalive.entities.Player;
import sfs2x.extensions.games.justalive.eventhandlers.client.*;
import sfs2x.extensions.games.justalive.eventhandlers.server.OnUserGoneHandler;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;
import sfs2x.extensions.games.justalive.helpers.UserHelper;

import java.util.List;

/**
 * Расширение игрового сервера
 * Область видимости - комната
 * Created by Denis on 20.05.2015.
 */
public class RoomController extends GameExtension {

    /**
     * API сервера
     */
    private SmartFoxServer sfs;

    /**
     * Объект игрового мира
     */
    private World world;

    /**
     * API для MMO игры
     * private ISFSMMOApi mmoApi;
     */

    /**
     * Инициализация игровой комнаты
     */
    public void init() {
        trace("initializing Just Alive server room...");

        this.sfs = SmartFoxServer.getInstance();
        this.world = new World(this);
        //this.mmoApi = this.sfs.getAPIManager().getMMOApi();

        registerEventHandlers();

        trace("server room initialized");
    }

    /**
     * Регистрация обработчиков событий
     */
    private void registerEventHandlers() {
        addRequestHandler("sendTransform", OnSendTransformRequestHandler.class);
        addRequestHandler("sendAnim", OnSendAnimationRequestHandler.class);
        addRequestHandler("getTime", OnGetTimeRequestHandler.class);
        addRequestHandler("shot", OnShotHandler.class);
        addRequestHandler("reload", OnReloadHandler.class);

        addRequestHandler("spawnMe", OnUserJoinHandler.class);
        addEventHandler(SFSEventType.USER_DISCONNECT, OnUserGoneHandler.class);
        addEventHandler(SFSEventType.USER_LEAVE_ROOM, OnUserGoneHandler.class);
    }

    /**
     * Отправка события "Пользователь был создан"
     *
     * @param player пользователь который был создан
     */
    public void sendInstantiatePlayer(Player player) {
        sendInstantiatePlayer(player.getSfsUser(), null);
        sendAmmoUpdatedFor(player);
    }

    /**
     * Отправка события "Пользователь был убит игроком"
     *
     * @param pl       текущий игрок
     * @param killerPl кем был убит
     */
    public void clientKilledByPlayer(Player pl, Player killerPl) {
        ISFSObject data = new SFSObject();
        data.putInt("id", pl.getSfsUser().getId());
        data.putInt("killerId", killerPl.getSfsUser().getId());

        sendAll("killed", data);
    }

    /**
     * Отправка события "Обновление здоровья"
     *
     * @param pl игрок для обновления
     */
    public void clientUpdateHealth(Player pl) {
        ISFSObject data = new SFSObject();
        data.putInt("id", pl.getSfsUser().getId());
        data.putInt("health", pl.getHealth());

        sendAll("health", data);
    }

    /**
     * Отправка события "Обновление статистики игрока"
     *
     * @param pl игрок для обновления
     */
    public void updatePlayerScore(Player pl) {
        ISFSObject data = new SFSObject();
        data.putInt("id", pl.getSfsUser().getId());
        data.putInt("score", pl.getScore());

        sendAll("score", data);
    }

    /**
     * Отправка события "Добавление предмета"
     *
     * @param item предмет для добавления
     */
    public void clientInstantiateItem(Item item, Player player) {
        ISFSObject data = item.toSFSObject();
        if (player == null) {
            sendAll("spawnItem", data);
        } else {
            send("spawnItem", data, player.getSfsUser());
        }
    }

    /**
     * Отправка события "Удаление  предмета"
     *
     * @param item   предмет
     * @param player пользователь который удалил предмет
     */
    public void clientRemoveItem(Item item, Player player) {
        ISFSObject data = item.toSFSObject();
        data.putInt("playerId", player.getSfsUser().getId());

        sendAll("removeItem", data);
    }

    /**
     * Отправка события "Пользователь выстрелил"
     *
     * @param pl пользователь который выстрелил
     */
    public void sendEnemyShotFiredBy(Player pl) {
        ISFSObject data = new SFSObject();
        data.putInt("id", pl.getSfsUser().getId());

        sendAll("enemyShotFired", data);
    }

    /**
     * Отправка события "Пользователь перезарядил оружие"
     *
     * @param player виновник торжества
     */
    public void clientReloaded(Player player) {
        ISFSObject data = new SFSObject();
        data.putInt("id", player.getSfsUser().getId());

        sendAll("reloaded", data);
    }

    /**
     * Отправка события "Обновление  количества патронов"
     *
     * @param player кому обновляем патроны
     */
    public void sendAmmoUpdatedFor(Player player) {
        ISFSObject data = new SFSObject();
        data.putInt("id", player.getSfsUser().getId());
        data.putInt("ammo", player.getWeapon().getAmmoCount());
        data.putInt("maxAmmo", 10);
        data.putInt("unloadedAmmo", player.getAmmoReserve());

        send("ammo", data, player.getSfsUser());
    }

    /**
     * Отправка события "Игрок был добавлен"
     *
     * @param addedUser     игрок который был добавлен
     */
    public void sendInstantiatePlayer(User addedUser, User targetUser) {
        Player player = world.getPlayer(addedUser);
        ISFSObject data = player.toSFSObject();
        if (targetUser == null) {
            List<User> users = getAllPlayers();
            if (!users.contains(addedUser)) {
                users.add(addedUser);
            }
            send("spawnPlayer", data, users);
        }
        else
            send("spawnPlayer", data, targetUser);
    }

    /**
     * Отправка сообщение "Пользователь покинул игру"
     *
     * @param player виновник торжества
     */
    public void leaveUser(Player player) {
        ISFSObject data = new SFSObject();
        data.putInt("id", player.getSfsUser().getId());
        sendAll("leavePlayer", data);
    }

    /**
     * Отправка сообщения всем игрокам в комнате
     *
     * @param cmd  команда
     * @param data данные для отправки
     */
    private void sendAll(String cmd, ISFSObject data) {
        List<User> userList = getAllPlayers();
        send(cmd, data, userList);
    }

    /**
     * Возвращает всех пользователей в комнате
     *
     * @return
     */
    private List<User> getAllPlayers() {
        Room currentRoom = RoomHelper.getCurrentRoom(this);
        return UserHelper.getRecipientsList(currentRoom);
    }

    /**
     * Высвобождение ресурсов
     */
    public void destroy() {
        this.world = null;
        super.destroy();
    }

    /**
     * Возвращает текуйщий игровой мир
     *
     * @return
     */
    public World getWorld() {
        return world;
    }

    /**
     * MMO ITEMS EXAMPLES
     *

     public int addWeaponShot(String weaponModel, double x, double y, double vx, double vy)
     {
     List<IMMOItemVariable> vars = new ArrayList();
     vars.add(new MMOItemVariable("iModel", weaponModel));
     vars.add(new MMOItemVariable("iType", "weapon"));

     MMOItem item = new MMOItem(vars);
     setMMOItemPosition(item, x, y);
     return item.getId();
     }

     private void setMMOItemPosition(BaseMMOItem item, double x, double y)
     {
     int intX = (int)Math.round(x);
     int intY = (int)Math.round(y);
     Vec3D pos = new Vec3D(intX, intY, 0);
     this.mmoApi.setMMOItemPosition(item, pos, getParentRoom());
     }

     public void setUserState(int userId, double x, double y, double z, double rotation, boolean fireClientEvent)
     {
     User user = this.room.getUserById(userId);
     if (user != null)
     {
     List<UserVariable> vars = new ArrayList();
     vars.add(new SFSUserVariable("x", Double.valueOf(x)));
     vars.add(new SFSUserVariable("y", Double.valueOf(y)));
     vars.add(new SFSUserVariable("z", Double.valueOf(z)));
     vars.add(new SFSUserVariable("r", Double.valueOf(rotation)));

     getApi().setUserVariables(user, vars, fireClientEvent, false);

     int intX = (int)Math.round(x);
     int intY = (int)Math.round(y);
     Vec3D pos = new Vec3D(intX, intY, 0);
     this.mmoApi.setUserPosition(user, pos, getParentRoom());
     }
     }
     */
}
