package sfs2x.extensions.games.justalive.entities;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import just.alive.server.dto.AbstractDto;
import sfs2x.extensions.games.justalive.entities.physics.Collider;
import sfs2x.extensions.games.justalive.entities.physics.Transform;

/**
 * Created by Denis on 20.05.2015.
 */
public class Player extends AbstractDto {
    public static final int maxHealth = 100;
    public static final int defaultAmmoReserve = 40;
    public static final int maxAmmoReserve = 50;
    private static final int defaultHitHP = 30;
    private User sfsUser;
    private Weapon weapon;
    private Transform transform;
    private Collider collider;
    private int health;
    private int score;
    private int ammoReserve;

    public boolean isDead()
    {
        return this.health <= 0;
    }

    public void removeHealth(int count)
    {
        this.health -= count;
    }

    public void hit()
    {
        removeHealth(defaultHitHP);
    }

    public User getSfsUser()
    {
        return this.sfsUser;
    }

    public Transform getTransform()
    {
        return this.transform;
    }

    public Player(User sfsUser)
    {
        this.sfsUser = sfsUser;
        this.weapon = new Weapon();
        this.transform = Transform.random();
        this.collider = new Collider(0.0D, 1.0D, 0.0D, 0.5D, 2.0D);
        health = 100;
        score = 0;
        ammoReserve = 40;
    }

    public Collider getCollider()
    {
        return this.collider;
    }

    public double getX()
    {
        return this.collider.getCenterx() + this.transform.getX();
    }

    public double getY()
    {
        return this.collider.getCentery() + this.transform.getY();
    }

    public double getZ()
    {
        return this.collider.getCenterz() + this.transform.getZ();
    }

    public int getHealth()
    {
        return this.health;
    }

    public void resurrect()
    {
        this.health = maxHealth;
        this.weapon.resetAmmo();
        this.ammoReserve = defaultAmmoReserve;
        this.transform = Transform.random();
    }

    /**
     * �������� ���� ������
     */
    public void addKillToScore()
    {
        this.score += 1;
    }

    public Weapon getWeapon()
    {
        return this.weapon;
    }

    public void reload()
    {
        if (this.ammoReserve == 0) {
            return;
        }
        if (this.weapon.isFullyLoaded()) {
            return;
        }
        int ammoUsedInReload = this.weapon.reload(this.ammoReserve);
        this.ammoReserve -= ammoUsedInReload;
    }

    public int getAmmoReserve()
    {
        return this.ammoReserve;
    }

    public void addAmmoToReserve(int i)
    {
        this.ammoReserve += i;
        if (this.ammoReserve > maxAmmoReserve) {
            this.ammoReserve = maxAmmoReserve;
        }
    }

    public void addHealth(int i)
    {
        this.health += i;
        if (this.health > maxHealth) {
            this.health = maxHealth;
        }
    }

    public boolean hasMaxAmmoInReserve()
    {
        return this.ammoReserve == maxAmmoReserve;
    }

    public boolean hasMaxHealth()
    {
        return this.health == maxHealth;
    }

    public int getScore()
    {
        return this.score;
    }
}
