package sfs2x.extensions.games.justalive.entities.physics;

import just.alive.server.dto.AbstractDto;

import java.util.Random;

/**
 * Created by Denis on 20.05.2015.
 */
public class Transform extends AbstractDto
{
    private double x;
    private double y;
    private double z;
    private double rotx;
    private double roty;
    private double rotz;
    private long timeStamp = 0L;
    private static Random rnd = new Random();

    public static Transform random()
    {
        Transform[] spawnPoints = getSpawnPoints();

        int i = rnd.nextInt(spawnPoints.length);
        return spawnPoints[i];
    }

    public static Transform randomWorld()
    {
        double x = rnd.nextDouble() * 70.0D + -35.0D;
        double z = rnd.nextDouble() * 75.0D + -70.0D;
        double y = 6.0D;
        return new Transform(x, y, z, 0.0D, 0.0D, 0.0D);
    }

    private static Transform[] getSpawnPoints()
    {
        Transform[] spawnPoints = new Transform[3];
        spawnPoints[0] = new Transform(25.0D, 6.0D, 3.0D, 0.0D, 0.0D, 0.0D);
        spawnPoints[1] = new Transform(-24.0D, 6.0D, -20.0D, 0.0D, 0.0D, 0.0D);
        spawnPoints[2] = new Transform(18.0D, 6.0D, -63.0D, 0.0D, 0.0D, 0.0D);
        return spawnPoints;
    }

    public Transform() {
    }

    public Transform(double x, double y, double z, double rotx, double roty, double rotz)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        this.rotx = rotx;
        this.roty = roty;
        this.rotz = rotz;
    }

    public double getRotx()
    {
        return this.rotx;
    }

    public double getRoty()
    {
        return this.roty;
    }

    public double getX()
    {
        return this.x;
    }

    public double getY()
    {
        return this.y;
    }

    public double getZ()
    {
        return this.z;
    }

    public void setTimeStamp(long timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public long getTimeStamp()
    {
        return this.timeStamp;
    }

    public void load(Transform another)
    {
        this.x = another.x;
        this.y = another.y;
        this.z = another.z;

        this.rotx = another.rotx;
        this.roty = another.roty;
        this.rotz = another.rotz;

        setTimeStamp(another.getTimeStamp());
    }

    public double distanceTo(Transform transform)
    {
        double dx = Math.pow(getX() - transform.getX(), 2.0D);
        double dy = Math.pow(getY() - transform.getY(), 2.0D);
        double dz = Math.pow(getZ() - transform.getZ(), 2.0D);
        return Math.sqrt(dx + dy + dz);
    }
}
