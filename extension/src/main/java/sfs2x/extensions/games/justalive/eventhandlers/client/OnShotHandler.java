package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import just.alive.server.handlers.AbstractClientRequestHandler;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;

/**
 * Created by Denis on 20.05.2015.
 */
public class OnShotHandler extends AbstractClientRequestHandler {
    @Override
    public void doHandle(User user, ISFSObject isfsObject) {
        RoomHelper.getWorld(this).processShot(user);
    }
}
