package sfs2x.extensions.games.justalive.helpers;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import com.smartfoxserver.v2.extensions.SFSExtension;
import sfs2x.extensions.games.justalive.RoomController;
import sfs2x.extensions.games.justalive.core.World;

/**
 * ��������������� ����� ��� ������ � ��������
 * Created by Denis on 20.05.2015.
 */
public class RoomHelper {

    /**
     * ���������� ������� �������
     *
     * @param handler ���������� �� �������� ����� ������� �������
     * @return ������� � ������� ��������������� ������� ����������
     */
    public static Room getCurrentRoom(BaseClientRequestHandler handler) {
        return handler.getParentExtension().getParentRoom();
    }

    /**
     * ���������� ������� �� ����������
     *
     * @param extension ���������� ������ �������� �������
     * @return �������
     */
    public static Room getCurrentRoom(SFSExtension extension) {
        return extension.getParentRoom();
    }

    /**
     * ���������� ������ �������� ����
     *
     * @param handler
     * @return
     */
    public static World getWorld(BaseClientRequestHandler handler) {
        RoomController ext = (RoomController) handler.getParentExtension();
        return ext.getWorld();
    }

    public static World getWorld(BaseServerEventHandler handler) {
        RoomController ext = (RoomController) handler.getParentExtension();
        return ext.getWorld();
    }
}
