package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import just.alive.server.handlers.AbstractClientRequestHandler;
import sfs2x.extensions.games.justalive.core.World;
import sfs2x.extensions.games.justalive.entities.physics.Transform;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;
import sfs2x.extensions.games.justalive.helpers.UserHelper;

import java.util.Date;
import java.util.List;

/**
 * Created by Denis on 20.05.2015.
 */

public class OnSendTransformRequestHandler extends AbstractClientRequestHandler {
    @Override
    public void doHandle(User user, ISFSObject isfsObject) {
        Transform receivedTransform = new Transform();
        receivedTransform.updateFromSFSObject(isfsObject);

        World world = RoomHelper.getWorld(this);

        Transform resultTransform = world.movePlayer(user, receivedTransform);
        if (resultTransform != null) {
            sendTransform(user, resultTransform);
        } else {
            sendRejectedTransform(user);
        }
    }

    private void sendTransform(User fromUser, Transform resultTransform) {
        long time = new Date().getTime();
        resultTransform.setTimeStamp(time);
        ISFSObject data = resultTransform.toSFSObject();
        data.putInt("id", fromUser.getId());

        Room currentRoom = RoomHelper.getCurrentRoom(this);
        List<User> userList = UserHelper.getRecipientsList(currentRoom);
        send("transform", data, userList, true);
    }

    private void sendRejectedTransform(User u) {
        ISFSObject data = RoomHelper.getWorld(this).getTransform(u).toSFSObject();
        data.putInt("id", u.getId());
        send("notransform", data, u, true);
    }
}
