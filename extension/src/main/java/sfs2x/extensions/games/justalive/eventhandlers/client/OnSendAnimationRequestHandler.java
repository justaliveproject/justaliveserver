package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import just.alive.server.handlers.AbstractClientRequestHandler;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;
import sfs2x.extensions.games.justalive.helpers.UserHelper;

/**
 * Created by Denis on 20.05.2015.
 */
public class OnSendAnimationRequestHandler extends AbstractClientRequestHandler {
    @Override
    public void doHandle(User user, ISFSObject isfsObject) {
        ISFSObject res = new SFSObject();
        res.putUtfString("msg", isfsObject.getUtfString("msg"));
        res.putInt("layer", isfsObject.getInt("layer").intValue());
        res.putInt("id", user.getId());
        Room currentRoom = RoomHelper.getCurrentRoom(this);
        send("anim", res, UserHelper.getRecipientsList(currentRoom, user));
    }
}