package sfs2x.extensions.games.justalive.entities;

import just.alive.server.dto.AbstractDto;

/**
 * Created by Denis on 20.05.2015.
 */
public class Weapon extends AbstractDto {
    public static final int maxAmmo = 10;
    private static final double shotTime = 600.0D;
    private static final double reloadTime = 1300.0D;
    private int ammoCount = 10;
    private long lastShotTime = 0L;
    private long lastReloadTime = 0L;

    public Weapon()
    {
        resetAmmo();
    }

    public void resetAmmo()
    {
        this.ammoCount = 10;
    }

    public int reload(int ammoReserve)
    {
        this.lastReloadTime = System.currentTimeMillis();
        int usedAmmo = maxAmmo - this.ammoCount;
        if (usedAmmo > ammoReserve) {
            usedAmmo = ammoReserve;
        }
        this.ammoCount += usedAmmo;
        return usedAmmo;
    }

    public void shoot()
    {
        if (!isAmmoEmpty())
        {
            this.ammoCount -= 1;
            this.lastShotTime = System.currentTimeMillis();
        }
    }

    public int getAmmoCount()
    {
        return this.ammoCount;
    }

    public boolean isAmmoEmpty(){
        return getAmmoCount() <= 0;
    }

    public boolean isFullyLoaded()
    {
        return this.ammoCount >= maxAmmo;
    }

    public boolean isReadyToFire()
    {
        if (isAmmoEmpty()) {
            return false;
        }
        if (this.lastReloadTime + reloadTime > System.currentTimeMillis()) {
            return false;
        }
        return this.lastShotTime + shotTime <= System.currentTimeMillis();
    }

    public boolean canReload()
    {
        if (isFullyLoaded()) {
            return false;
        }
        if (this.lastReloadTime + reloadTime > System.currentTimeMillis()) {
            return false;
        }
        return this.lastShotTime + shotTime <= System.currentTimeMillis();
    }
}
