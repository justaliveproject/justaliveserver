package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.exceptions.SFSException;
import just.alive.server.handlers.AbstractServerEventHandler;

/**
 * Created by Denis on 20.05.2015.
 */
public class PublicMessageHandler extends AbstractServerEventHandler {

    @Override
    public void doHandle(ISFSEvent isfsEvent) throws SFSException {
        trace("PUBLIC MESSAGE");
    }
}
