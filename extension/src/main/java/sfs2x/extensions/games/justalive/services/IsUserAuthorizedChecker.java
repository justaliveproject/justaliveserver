package sfs2x.extensions.games.justalive.services;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.extensions.SFSExtension;
import just.alive.server.exceptions.UnauthorizedException;
import just.alive.server.repositories.UserRepository;
import just.alive.server.services.security.AbstractChecker;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Denis on 23.05.2015.
 */
public class IsUserAuthorizedChecker extends AbstractChecker {
    @Autowired
    public UserRepository userRepository;

    public void check(User user, SFSExtension extension) throws UnauthorizedException {
        if(userRepository.findByEmail(user.getVariable("email").toString()) == null){
            logAndThrow(new UnauthorizedException("You don't have the permission to access this handler!"));
        }
    }
}
