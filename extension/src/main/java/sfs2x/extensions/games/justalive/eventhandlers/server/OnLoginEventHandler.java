package sfs2x.extensions.games.justalive.eventhandlers.server;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import just.alive.server.handlers.AbstractServerEventHandler;

/**
 * Created by Denis on 20.05.2015.
 */

public class OnLoginEventHandler extends AbstractServerEventHandler {
    @Override
    public void doHandle(ISFSEvent isfsEvent) throws SFSException {
        ISFSObject outData = (ISFSObject)isfsEvent.getParameter(SFSEventParam.LOGIN_OUT_DATA);
        outData.putSFSObject("success", SFSObject.newFromObject(true));
    }
}
