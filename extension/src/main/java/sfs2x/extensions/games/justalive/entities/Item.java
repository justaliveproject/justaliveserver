package sfs2x.extensions.games.justalive.entities;

import just.alive.server.dto.AbstractDto;
import sfs2x.extensions.games.justalive.entities.physics.Transform;

/**
 * Created by Denis on 20.05.2015.
 */
public class Item extends AbstractDto {
    public static final double touchDistance = 3.0D;
    private int id;
    private Transform transform;
    private ItemType itemType;

    public Item(int id, Transform transform, ItemType itemType)
    {
        this.id = id;
        this.transform = transform;
        this.itemType = itemType;
    }

    public ItemType getItemType()
    {
        return this.itemType;
    }

    public boolean isClose(Transform newTransform)
    {
        double d = newTransform.distanceTo(this.transform);
        return d <= touchDistance;
    }

    public int getId() {
        return id;
    }
}
