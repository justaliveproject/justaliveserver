package sfs2x.extensions.games.justalive;

import com.smartfoxserver.v2.components.login.ILoginAssistantPlugin;
import com.smartfoxserver.v2.components.login.LoginAssistantComponent;
import com.smartfoxserver.v2.components.login.LoginData;
import com.smartfoxserver.v2.components.signup.PasswordMode;
import com.smartfoxserver.v2.components.signup.RecoveryMode;
import com.smartfoxserver.v2.components.signup.SignUpAssistantComponent;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.ExtensionLogLevel;
import com.smartfoxserver.v2.security.DefaultPermissionProfile;
import just.alive.server.GameExtension;
import sfs2x.extensions.games.justalive.eventhandlers.server.OnLoginEventHandler;
import sfs2x.extensions.games.justalive.eventhandlers.client.PublicMessageHandler;
import sfs2x.extensions.games.justalive.eventhandlers.server.OnUserGoneHandler;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

/**
 * ���������� �������� �������
 * ������� ��������� - ����
 * Created by Denis on 20.05.2015.
 */
public class ServerController extends GameExtension {

    private LoginAssistantComponent lac;
    private SignUpAssistantComponent suac;

    private String emailSignUpFrom;
    private String emailRecoveryFrom;
    private String emailSignUpSubject;
    private String emailRecoverySubject;
    private String emailSignUpTemplatePath;
    private String emailRecoveryTemplatePath;

    /**
     * ������������� �������.
     * ����������� ������������ �������
     */
    public void init() {
        readProperties();

        initializeLogInAssistant();
        initializeSignUpAssistant();

        addEventHandler(SFSEventType.USER_LOGIN, OnLoginEventHandler.class);
        addEventHandler(SFSEventType.USER_LOGOUT, OnUserGoneHandler.class);
        addEventHandler(SFSEventType.__TRACE_MESSAGE, PublicMessageHandler.class);
    }

    private void readProperties() {
        Properties properties = getConfigProperties();
        emailSignUpFrom = properties.getProperty("emailSignUpFrom");
        emailRecoveryFrom = properties.getProperty("emailRecoveryFrom");
        emailSignUpSubject = properties.getProperty("emailSignUpSubject");
        emailRecoverySubject = properties.getProperty("emailRecoverySubject");
        emailSignUpTemplatePath = properties.getProperty("emailSignUpTemplatePath");
        emailRecoveryTemplatePath = properties.getProperty("emailRecoveryTemplatePath");
    }

    private void initializeSignUpAssistant() {
        suac = new SignUpAssistantComponent();
        suac.getConfig().emailField = "user_email";
        suac.getConfig().usernameField = "user_name";
        suac.getConfig().passwordField = "user_password";
        suac.getConfig().passwordMode = PasswordMode.MD5;

        suac.getConfig().activationCodeField = "act_code";

        suac.getConfig().emailResponse.isActive = true;
        suac.getConfig().emailResponse.fromAddress = emailSignUpFrom;
        suac.getConfig().emailResponse.subject = emailSignUpSubject;
        suac.getConfig().emailResponse.template = emailSignUpTemplatePath;

        suac.getConfig().passwordRecovery.isActive = true;
        suac.getConfig().passwordRecovery.mode = RecoveryMode.GENERATE_NEW;
        suac.getConfig().passwordRecovery.email.fromAddress = emailRecoveryFrom;
        suac.getConfig().passwordRecovery.email.subject = emailRecoverySubject;
        suac.getConfig().passwordRecovery.email.template = emailRecoveryTemplatePath;

        addRequestHandler(SignUpAssistantComponent.COMMAND_PREFIX, suac);
    }

    private void initializeLogInAssistant() {
        lac = new LoginAssistantComponent(this);
        lac.getConfig().userNameField = "user_email";
        lac.getConfig().passwordField = "user_password";
        lac.getConfig().nickNameField = "user_name";
        lac.getConfig().extraFields = Arrays.asList("is_moderator");
        lac.getConfig().postProcessPlugin = new ILoginAssistantPlugin()
        {
            public void execute(LoginData loginData)
            {
                ISFSObject fields = loginData.extraFields;
                boolean isMod = fields.getUtfString("is_moderator").equalsIgnoreCase("Y");
                if (isMod)
                    loginData.session.setProperty("$permission", DefaultPermissionProfile.MODERATOR);
                else
                    loginData.session.setProperty("$permission", DefaultPermissionProfile.STANDARD);
            }
        };
    }
}
