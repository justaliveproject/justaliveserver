package sfs2x.extensions.games.justalive.eventhandlers.server;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.exceptions.SFSException;
import just.alive.server.handlers.AbstractServerEventHandler;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;

/**
 * Created by Denis on 20.05.2015.
 */
public class OnUserGoneHandler extends AbstractServerEventHandler {
    @Override
    public void doHandle(ISFSEvent isfsEvent) throws SFSException {
        User user = (User)isfsEvent.getParameter(SFSEventParam.USER);
        RoomHelper.getWorld(this).userLeft(user);
    }
}
