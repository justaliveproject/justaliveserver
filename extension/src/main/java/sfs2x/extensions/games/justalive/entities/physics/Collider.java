package sfs2x.extensions.games.justalive.entities.physics;

import just.alive.server.dto.AbstractDto;

/**
 * Created by Denis on 20.05.2015.
 */
public class Collider extends AbstractDto {
    private double centerx;
    private double centery;
    private double centerz;
    private double radius;
    private double height;

    public Collider(double x, double y, double z, double radius, double height)
    {
        this.centerx = x;
        this.centery = y;
        this.centerz = z;

        this.radius = radius;
        this.height = height;
    }

    public double getCenterx()
    {
        return this.centerx;
    }

    public double getCentery()
    {
        return this.centery;
    }

    public double getCenterz()
    {
        return this.centerz;
    }

    public double getRadius()
    {
        return this.radius;
    }

    public double getHeight()
    {
        return this.height;
    }
}
