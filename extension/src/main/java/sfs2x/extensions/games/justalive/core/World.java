package sfs2x.extensions.games.justalive.core;

import com.smartfoxserver.v2.entities.User;
import sfs2x.extensions.games.justalive.RoomController;
import sfs2x.extensions.games.justalive.entities.Item;
import sfs2x.extensions.games.justalive.entities.ItemType;
import sfs2x.extensions.games.justalive.entities.Player;
import sfs2x.extensions.games.justalive.entities.physics.Transform;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by Denis on 20.05.2015.
 */
public class World {

    public static final double minX = -35.0D;
    public static final double maxX = 35.0D;
    public static final double minZ = -70.0D;
    public static final double maxZ = 5.0D;
    private static final int maxSpawnedItems = 6;
    private static final int maxItemsOfSingleType = 3;
    private static Random rnd = new Random();
    private int itemId = 0;
    private List<Player> players = new ArrayList();
    private List<Item> items = new ArrayList();

    private RoomController roomController;

    public World(RoomController roomController) {
        this.roomController = roomController;
        rnd.setSeed(new Date().getTime());
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    public void spawnItemsFor(User user) {
        Player player = getPlayer(user);
        if (player != null) {
            for (Item item : this.items){
                this.roomController.clientInstantiateItem(item, player);
            }
        }
        int itemsCount = rnd.nextInt(6);
        int healthItemsCount = itemsCount / 2;
        int hc = 0;
        this.roomController.trace("Spawn " + itemsCount + " items.");
        for (int i = 0; i < itemsCount; i++) {
            ItemType itemType = hc++ < healthItemsCount ? ItemType.HealthPack : ItemType.Ammo;
            if (!hasMaxItems(itemType)) {
                Item item = new Item(this.itemId++, Transform.randomWorld(), itemType);
                this.items.add(item);
                this.roomController.clientInstantiateItem(item, null);
            }
        }
    }

    private boolean hasMaxItems(ItemType itemType) {
        int counter = 0;
        for (Item item : this.items) {
            if (item.getItemType() == itemType) {
                counter++;
            }
        }
        return counter > 3;
    }

    public boolean addOrRespawnPlayer(User user) {
        Player player = getPlayer(user);
        if (player == null) {
            player = new Player(user);
            this.players.add(player);
            this.roomController.sendInstantiatePlayer(player);
            return true;
        }
        player.resurrect();
        this.roomController.sendInstantiatePlayer(player);
        return false;
    }

    public Transform movePlayer(User u, Transform newTransform) {
        Player player = getPlayer(u);
        if (player.isDead()) {
            return player.getTransform();
        }
        if (isValidNewTransform(player, newTransform)) {
            player.getTransform().load(newTransform);

            checkItem(player, newTransform);

            return newTransform;
        }
        return null;
    }

    private void checkItem(Player player, Transform newTransform) {
        for (Object itemObj : this.items.toArray()) {
            Item item = (Item) itemObj;
            if (item.isClose(newTransform)) {
                try {
                    useItem(player, item);
                } catch (Throwable e) {
                    this.roomController.trace("Exception using item " + e.getMessage());
                }
                return;
            }
        }
    }

    private void useItem(Player player, Item item) {
        if (item.getItemType() == ItemType.Ammo) {
            if (player.hasMaxAmmoInReserve()) {
                return;
            }
            player.addAmmoToReserve(20);
            this.roomController.sendAmmoUpdatedFor(player);
        } else if (item.getItemType() == ItemType.HealthPack) {
            if (player.hasMaxHealth()) {
                return;
            }
            player.addHealth(33);
            this.roomController.clientUpdateHealth(player);
        }
        this.roomController.clientRemoveItem(item, player);
        this.items.remove(item);
    }

    public Transform getTransform(User u) {
        Player player = getPlayer(u);
        return player.getTransform();
    }

    private boolean isValidNewTransform(Player player, Transform newTransform) {
        return true;
    }

    public Player getPlayer(User u) {
        for (Player player : this.players) {
            if (player.getSfsUser().getId() == u.getId()) {
                return player;
            }
        }
        return null;
    }

    public void processShot(User fromUser) {
        Player player = getPlayer(fromUser);
        if (player.isDead()) {
            return;
        }
        if (!player.getWeapon().isReadyToFire()) {
            return;
        }
        player.getWeapon().shoot();

        this.roomController.sendAmmoUpdatedFor(player);
        this.roomController.sendEnemyShotFiredBy(player);
        for (Player pl : this.players) {
            if (pl != player) {
                boolean canHit = checkHit(player, pl);
                if (canHit) {
                    playerHit(player, pl);
                    return;
                }
            }
        }
    }

    public void processReload(User fromUser) {
        Player player = getPlayer(fromUser);
        if (player.isDead()) {
            return;
        }
        if (player.getAmmoReserve() == 0) {
            return;
        }
        if (!player.getWeapon().canReload()) {
            return;
        }
        player.reload();
        this.roomController.clientReloaded(player);
        this.roomController.sendAmmoUpdatedFor(player);
    }

    private boolean checkHit(Player player, Player enemy) {
        if (enemy.isDead()) {
            return false;
        }
        double radius = enemy.getCollider().getRadius();
        double height = enemy.getCollider().getHeight();
        double myAngle = player.getTransform().getRoty();
        double vertAngle = player.getTransform().getRotx();


        double normalAngle = normAngle(450.0D - myAngle);


        double difx = enemy.getX() - player.getX();
        double difz = enemy.getZ() - player.getZ();

        double ang;
        if (difx == 0.0D) {
            ang = 90.0D;
        } else {
            ang = Math.toDegrees(Math.atan(Math.abs(difz / difx)));
        }
        if (difx <= 0.0D) {
            if (difz <= 0.0D) {
                ang += 180.0D;
            } else {
                ang = 180.0D - ang;
            }
        } else if (difz <= 0.0D) {
            ang = 360.0D - ang;
        }
        ang = normAngle(ang);


        double angDif = Math.abs(ang - normalAngle);
        double d = Math.sqrt(difx * difx + difz * difz);
        double maxDif = Math.toDegrees(Math.atan(radius / d));
        if (angDif > maxDif) {
            return false;
        }
        if (vertAngle > 90.0D) {
            vertAngle = 360.0D - vertAngle;
        } else {
            vertAngle = -vertAngle;
        }
        double h = d * Math.tan(Math.toRadians(vertAngle));
        double dif = enemy.getTransform().getY() - player.getTransform().getY() - h + 0.3D;
        return !((dif < 0.0D) || (dif > height));
    }

    private double normAngle(double a) {
        if (a >= 360.0D) {
            return a - 360.0D;
        }
        return a;
    }

    private void playerHit(Player fromPlayer, Player pl) {
        pl.hit();
        if (pl.isDead()) {
            fromPlayer.addKillToScore();
            this.roomController.updatePlayerScore(fromPlayer);
            this.roomController.clientKilledByPlayer(pl, fromPlayer);
        } else {
            this.roomController.clientUpdateHealth(pl);
        }
    }

    public void userLeft(User user) {
        Player player = getPlayer(user);
        if (player == null) {
            return;
        }
        this.players.remove(player);
        this.roomController.leaveUser(player);
    }
}
