package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import just.alive.server.annotations.Security;
import just.alive.server.handlers.AbstractClientRequestHandler;
import sfs2x.extensions.games.justalive.RoomController;
import sfs2x.extensions.games.justalive.entities.Player;
import sfs2x.extensions.games.justalive.core.World;
import sfs2x.extensions.games.justalive.helpers.RoomHelper;
import sfs2x.extensions.games.justalive.services.IsUserAuthorizedChecker;

/**
 * Created by Denis on 20.05.2015.
 */
@Security(customChecker = IsUserAuthorizedChecker.class)
public class OnUserJoinHandler extends AbstractClientRequestHandler
{
    @Override
    public void doHandle(User user, ISFSObject isfsObject) {
        World world = RoomHelper.getWorld(this);
        boolean newPlayer = world.addOrRespawnPlayer(user);
        if (newPlayer) {
            sendOtherPlayersInfoFor(user);
        }
        world.spawnItemsFor(user);
    }

    private void sendOtherPlayersInfoFor(User recipient)
    {
        World world = RoomHelper.getWorld(this);
        for (Player player : world.getPlayers()) {
            if (!player.isDead()) {
                if (player.getSfsUser().getId() != recipient.getId())
                {
                    RoomController ext = (RoomController)getParentExtension();
                    ext.sendInstantiatePlayer(player.getSfsUser(), recipient);
                }
            }
        }
    }
}
