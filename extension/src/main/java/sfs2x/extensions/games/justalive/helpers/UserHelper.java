package sfs2x.extensions.games.justalive.helpers;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;

import java.util.List;

/**
 * ��������������� ����� ��� ������ � ��������������
 * Created by Denis on 20.05.2015.
 */
public class UserHelper {
    /**
     * ���������� ������ ������������� � ������� �� ����������� ������
     * @param room ������� �� ������� ������� ������������
     * @param exceptUser ������������ �������� �� ����� ����������
     * @return ������ ������������� � �������
     */
    public static List<User> getRecipientsList(Room room, User exceptUser)
    {
        List<User> users = room.getUserList();
        if (exceptUser != null) {
            users.remove(exceptUser);
        }
        return users;
    }

    /**
     * ���������� ������ ������������� � �������
     * @param currentRoom ������� �� ������� ����� �������������
     * @return ������ �������������
     */
    public static List<User> getRecipientsList(Room currentRoom)
    {
        return getRecipientsList(currentRoom, null);
    }
}
