package sfs2x.extensions.games.justalive.eventhandlers.client;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import just.alive.server.handlers.AbstractClientRequestHandler;

import java.util.Date;

/**
 * Created by Denis on 20.05.2015.
 */
public class OnGetTimeRequestHandler extends AbstractClientRequestHandler {
    @Override
    public void doHandle(User user, ISFSObject isfsObject) {
        ISFSObject res = new SFSObject();
        Date date = new Date();
        res.putLong("t", date.getTime());
        send("time", res, user);
    }
}