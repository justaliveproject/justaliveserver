package just.alive.server.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Denis on 24.05.2015.
 */
public abstract class AbstractService {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Log and throw the exception next
     *
     * @param e exception
     */
    public void logAndThrow(Throwable e){
        logger.error(e.getMessage(), e);
        throw new RuntimeException(e);
    }

    /**
     * Log and throw the exception next
     *
     * @param e exception
     */
    public void logAndThrow(RuntimeException e) {
        logger.error(e.getMessage(), e);
        throw e;
    }

    public void logAndThrow(String message) throws RuntimeException {
        logAndThrow(new RuntimeException(message));
    }
}
