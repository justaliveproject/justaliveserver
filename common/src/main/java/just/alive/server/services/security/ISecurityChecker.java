package just.alive.server.services.security;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.extensions.SFSExtension;
import just.alive.server.exceptions.UnauthorizedException;

/**
 * Created by Denis on 24.05.2015.
 */
public interface ISecurityChecker {
    void check(User user, SFSExtension extension) throws UnauthorizedException;
}
