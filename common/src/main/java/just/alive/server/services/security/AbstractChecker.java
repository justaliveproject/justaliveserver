package just.alive.server.services.security;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.extensions.SFSExtension;
import just.alive.server.annotations.Security;
import just.alive.server.exceptions.MetadataException;
import just.alive.server.exceptions.UnauthorizedException;
import just.alive.server.handlers.IAbstractHandler;
import just.alive.server.services.AbstractService;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Denis on 24.05.2015.
 */
@Component
public abstract class AbstractChecker extends AbstractService implements ISecurityChecker {

    private static class AuthStrategy {
        public ISecurityChecker service;
        public Boolean authRequired;
    }

    private static final Map<Class<? extends IAbstractHandler>, AuthStrategy> authCache =
            new ConcurrentHashMap<Class<? extends IAbstractHandler>, AuthStrategy>();

    public static void checkAuthIfRequired(final IAbstractHandler handler, User user) {
        if (!authCache.containsKey(handler.getClass())) {
            Security security = handler.getClass().getAnnotation(Security.class);
            AuthStrategy authStrategy = new AuthStrategy();
            try {
                authStrategy.service = (security == null) ? new ISecurityChecker() {
                    public void check(User user, SFSExtension extension) throws UnauthorizedException {
                        //dummy security check
                    }
                } : (ISecurityChecker) security.customChecker().newInstance();
                authStrategy.authRequired = (security != null) && security.isNeedCheck();
                authCache.put(handler.getClass(), authStrategy);
            } catch (Exception e) {
                throw new MetadataException(e);
            }
        }
        if (authCache.containsKey(handler.getClass()) && authCache.get(handler.getClass()).authRequired) {
            authCache.get(handler.getClass()).service.check(user, handler.getParentExtension());
        }
    }

    public abstract void check(User user, SFSExtension extension) throws UnauthorizedException;
}
