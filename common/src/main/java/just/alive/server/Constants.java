package just.alive.server;

/**
 * Created by Denis on 23.05.2015.
 */
public interface Constants {
    interface Event {
        String PRIVATE_MESSAGE_SEND_ERROR = "privateMessageSendError";
        String PRIVATE_MESSAGE_SEND_OK = "privateMessageSendOK";
    }

    interface Request {
        String PRIVATE_MESSAGE = "privateMessage";
    }

    interface Param {
        String FROM_USER_ID = "fromUserId";
        String TO_USER_ID = "toUserId";
        String MESSAGE = "message";
        String ERROR_CODE = "errorCode";
    }

    interface ErrorCode {
        Integer MESSAGE_CANNOT_BE_EMPTY = 5;
        Integer USER_NOT_FOUND = 6;
        just.alive.server.exceptions.ErrorCode LOGIN_TYPE_EMPTY = new just.alive.server.exceptions.ErrorCode(32003);
        just.alive.server.exceptions.ErrorCode LOGIN_TYPE_UNKNOWN = new just.alive.server.exceptions.ErrorCode(32004);
        just.alive.server.exceptions.ErrorCode LOGIN_BAD_PASSWORD = new just.alive.server.exceptions.ErrorCode(32006);
        just.alive.server.exceptions.ErrorCode LOGIN_BAD_USERNAME = new just.alive.server.exceptions.ErrorCode(32007);
    }

    interface Auth {
        interface LoginType {
            int LOGIN_WITH_GUEST_ACCOUNT = 0;
            int LOGIN_USING_TOKEN = 1;
            int LOGIN_USING_EMAIL = 2;
        }

        interface LoginParam {
            String LOGIN_TYPE = "loginType";
        }
    }
}
