package just.alive.server.annotations.serialization;

import java.lang.annotation.*;

/**
 * Created by Denis on 24.05.2015.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Serialize {
    boolean serialize() default true;

    boolean deserialize() default true;

    String name() default "";

    String[] options() default {};

    final class DEFAULT {
        public static Serialize get() {
            return new Serialize() {
                public boolean serialize() {
                    return true;
                }

                public boolean deserialize() {
                    return true;
                }

                public String name() {
                    return "";
                }

                public String[] options() {
                    return new String[0];
                }

                public Class<? extends Annotation> annotationType() {
                    return Serialize.class;
                }
            };
        }
    }
}