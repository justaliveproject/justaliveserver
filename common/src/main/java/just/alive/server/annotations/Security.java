package just.alive.server.annotations;

import just.alive.server.services.security.AbstractChecker;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Denis on 24.05.2015.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    boolean isNeedCheck() default true;
    Class<? extends AbstractChecker> customChecker() default AbstractChecker.class;
}