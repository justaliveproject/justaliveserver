package just.alive.server.annotations.serialization;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Denis on 24.05.2015.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SerializeStrategy {
    enum Strategy {
        ANNOTATED_FIELDS,
        ALL_FIELDS;
        public static final Strategy DEFAULT = ALL_FIELDS;
    }
    Strategy type() default Strategy.ALL_FIELDS;
}