package just.alive.server.entities;

import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.buddylist.BuddyProperties;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.Zone;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.mmo.BaseMMOItem;
import com.smartfoxserver.v2.mmo.MMORoom;
import com.smartfoxserver.v2.util.IDisconnectionReason;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.List;
import java.util.Map;

/**
 * Created by Denis on 24.05.2015.
 */
@Entity
public class UserModel extends AbstractEntity implements User {

    @Column
    private String email;

    public int getId() {
        return 0;
    }

    public ISession getSession() {
        return null;
    }

    public String getIpAddress() {
        return null;
    }

    public String getName() {
        return null;
    }

    public BuddyProperties getBuddyProperties() {
        return null;
    }

    public void setName(String s) {

    }

    public boolean isLocal() {
        return false;
    }

    public boolean isNpc() {
        return false;
    }

    public long getLoginTime() {
        return 0;
    }

    public void setLastLoginTime(long l) {

    }

    public Room getLastJoinedRoom() {
        return null;
    }

    public List<Room> getJoinedRooms() {
        return null;
    }

    public void addJoinedRoom(Room room) {

    }

    public void removeJoinedRoom(Room room) {

    }

    public boolean isJoinedInRoom(Room room) {
        return false;
    }

    public void addCreatedRoom(Room room) {

    }

    public void removeCreatedRoom(Room room) {

    }

    public List<Room> getCreatedRooms() {
        return null;
    }

    public void subscribeGroup(String s) {

    }

    public void unsubscribeGroup(String s) {

    }

    public List<String> getSubscribedGroups() {
        return null;
    }

    public boolean isSubscribedToGroup(String s) {
        return false;
    }

    public Zone getZone() {
        return null;
    }

    public void setZone(Zone zone) {

    }

    public int getPlayerId() {
        return 0;
    }

    public int getPlayerId(Room room) {
        return 0;
    }

    public void setPlayerId(int i, Room room) {

    }

    public Map<Room, Integer> getPlayerIds() {
        return null;
    }

    public boolean isPlayer() {
        return false;
    }

    public boolean isSpectator() {
        return false;
    }

    public boolean isPlayer(Room room) {
        return false;
    }

    public boolean isSpectator(Room room) {
        return false;
    }

    public boolean isJoining() {
        return false;
    }

    public void setJoining(boolean b) {

    }

    public boolean isConnected() {
        return false;
    }

    public void setConnected(boolean b) {

    }

    public boolean isSuperUser() {
        return false;
    }

    public int getMaxAllowedVariables() {
        return 0;
    }

    public void setMaxAllowedVariables(int i) {

    }

    public Object getProperty(Object o) {
        return null;
    }

    public void setProperty(Object o, Object o1) {

    }

    public boolean containsProperty(Object o) {
        return false;
    }

    public void removeProperty(Object o) {

    }

    public int getOwnedRoomsCount() {
        return 0;
    }

    public boolean isBeingKicked() {
        return false;
    }

    public void setBeingKicked(boolean b) {

    }

    public short getPrivilegeId() {
        return 0;
    }

    public void setPrivilegeId(short i) {

    }

    public UserVariable getVariable(String s) {
        return null;
    }

    public List<UserVariable> getVariables() {
        return null;
    }

    public void setVariable(UserVariable userVariable) throws SFSVariableException {

    }

    public void setVariables(List<UserVariable> list) throws SFSVariableException {

    }

    public void removeVariable(String s) {

    }

    public boolean containsVariable(String s) {
        return false;
    }

    public int getVariablesCount() {
        return 0;
    }

    public int getBadWordsWarnings() {
        return 0;
    }

    public void setBadWordsWarnings(int i) {

    }

    public int getFloodWarnings() {
        return 0;
    }

    public void setFloodWarnings(int i) {

    }

    public long getLastRequestTime() {
        return 0;
    }

    public void setLastRequestTime(long l) {

    }

    public void updateLastRequestTime() {

    }

    public ISFSArray getUserVariablesData() {
        return null;
    }

    public ISFSArray toSFSArray(Room room) {
        return null;
    }

    public ISFSArray toSFSArray() {
        return null;
    }

    public void disconnect(IDisconnectionReason iDisconnectionReason) {

    }

    public String getDump() {
        return null;
    }

    public int getReconnectionSeconds() {
        return 0;
    }

    public void setReconnectionSeconds(int i) {

    }

    public List<User> getLastProxyList() {
        return null;
    }

    public void setLastProxyList(List<User> list) {

    }

    public List<BaseMMOItem> getLastMMOItemsList() {
        return null;
    }

    public void setLastMMOItemsList(List<BaseMMOItem> list) {

    }

    public MMORoom getCurrentMMORoom() {
        return null;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
