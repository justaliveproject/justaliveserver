package just.alive.server.repositories;

import just.alive.server.entities.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Denis on 24.05.2015.
 */
public interface UserRepository extends CrudRepository<UserModel, Long> {
    UserModel findByEmail(String email);
}
