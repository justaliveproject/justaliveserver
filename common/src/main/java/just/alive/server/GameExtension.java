package just.alive.server;

import com.smartfoxserver.v2.extensions.SFSExtension;
import just.alive.server.configuration.ApplicationContextConfiguration;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by ����� ������� on 23.05.2015.
 */
@Component
public abstract class GameExtension extends SFSExtension {
    private static final ApplicationContext applicationContext = createDefaultContext();

    private static ApplicationContext createDefaultContext() {
        return new AnnotationConfigApplicationContext(ApplicationContextConfiguration.class);
    }

    public GameExtension(){
        AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();
        bpp.setBeanFactory(applicationContext.getAutowireCapableBeanFactory());
        bpp.processInjection(this);
    }

    public ApplicationContext getAppContext(){
        return applicationContext;
    }
}
