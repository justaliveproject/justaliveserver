package just.alive.server.exceptions;

/**
 * Created by Denis on 24.05.2015.
 */
public class UnauthorizedException extends RuntimeException {
    public UnauthorizedException(String s) {
        super(s);
    }
    public UnauthorizedException(Throwable throwable) {
        super(throwable);
    }
}