package just.alive.server.exceptions;

/**
 * Created by Denis on 24.05.2015.
 */
public class MetadataException extends RuntimeException {
    public MetadataException(Throwable t) {
        super(t);
    }
    public MetadataException(String message) {
        super(message);
    }
}

