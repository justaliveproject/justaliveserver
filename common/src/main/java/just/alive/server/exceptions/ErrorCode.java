package just.alive.server.exceptions;

import com.smartfoxserver.v2.exceptions.IErrorCode;

/**
 * Created by Denis on 23.05.2015.
 */
public class ErrorCode implements IErrorCode {

    private short errorCode = 0;

    public ErrorCode(Integer errorCode) {
        this.errorCode = errorCode.shortValue();
    }

    public ErrorCode(short errorCode) {
        this.errorCode = errorCode;
    }

    public short getId() {
        return errorCode;
    }

    @Override
    public String toString() {
        return String.valueOf(getId());
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ErrorCode && ((ErrorCode) o).getId() == getId());
    }

    public static IErrorCode valueOf(String message) {
        return new ErrorCode(Integer.valueOf(message));
    }
}