package just.alive.server.handlers;

import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import com.smartfoxserver.v2.extensions.SFSExtension;
import just.alive.server.exceptions.UnauthorizedException;
import just.alive.server.services.security.AbstractChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Denis on 24.05.2015.
 */
@Instantiation(value = Instantiation.InstantiationMode.SINGLE_INSTANCE)
public abstract class AbstractClientRequestHandler extends BaseClientRequestHandler implements IAbstractHandler {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public SFSExtension getParentExtension() {
        return super.getParentExtension();
    }

    public final void handleClientRequest(User user, ISFSObject isfsObject) {
        try {
            AbstractChecker.checkAuthIfRequired(this, user);
            doHandle(user, isfsObject);
        } catch (UnauthorizedException ua) {
            logger.error("User unauthorized: " + ua.getMessage());
        }
    }

    public abstract void doHandle(User user, ISFSObject isfsObject);
}
