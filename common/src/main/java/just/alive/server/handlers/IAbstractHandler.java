package just.alive.server.handlers;

import com.smartfoxserver.v2.extensions.SFSExtension;

/**
 * Created by Denis on 24.05.2015.
 */
public interface IAbstractHandler {
    SFSExtension getParentExtension();
}
