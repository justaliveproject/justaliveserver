package just.alive.server.utils;

import java.util.*;

/**
 * Created by Denis on 24.05.2015.
 */
public class TypesUtil {
    public TypesUtil() {
    }

    public static boolean isInt(Class<?> type) {
        return Integer.class.isAssignableFrom(type) || type.isPrimitive() && type.toString().equals("int");
    }

    public static boolean isDouble(Class<?> type) {
        return Double.class.isAssignableFrom(type) || type.isPrimitive() && type.toString().equals("double");
    }

    public static boolean isFloat(Class<?> type) {
        return Float.class.isAssignableFrom(type) || type.isPrimitive() && type.toString().equals("float");
    }

    public static boolean isBoolean(Class<?> type) {
        return Boolean.class.isAssignableFrom(type) || type.isPrimitive() && type.toString().equals("boolean");
    }

    public static boolean isString(Class<?> type) {
        return String.class.isAssignableFrom(type);
    }

    public static boolean isLong(Class<?> type) {
        return Long.class.isAssignableFrom(type) || type.isPrimitive() && type.toString().equals("long");
    }

    public static Class<?> getGenericType(Class<?> objectType) {
        return isInt(objectType)?Integer.TYPE:(isBoolean(objectType)?Boolean.TYPE:(isDouble(objectType)?Double.TYPE:(isLong(objectType)?Long.TYPE:(isFloat(objectType)?Float.TYPE:objectType))));
    }

    public static boolean isDate(Class<?> type) {
        return Date.class.isAssignableFrom(type);
    }

    public static Collection instantiateCollection(Class<? extends Collection> collClass) {
        if(List.class.isAssignableFrom(collClass)) {
            return new ArrayList();
        } else if(Set.class.isAssignableFrom(collClass)) {
            return new HashSet();
        } else {
            throw new RuntimeException("Cannot instantiate collection of a class: " + collClass + ": Not supported!");
        }
    }
}
