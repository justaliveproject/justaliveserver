package just.alive.server.utils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;

import java.util.*;

/**
 * Created by Denis on 24.05.2015.
 */
public class EnumUtil {
    private static final Map<Class<? extends Enum>, Set<String>> cache = new HashMap();

    public EnumUtil() {
    }

    public static boolean enumContains(Class<? extends Enum> enumClass, String value) {
        if(!cache.containsKey(enumClass)) {
            HashSet options = new HashSet();
            Enum[] arr$ = enumClass.getEnumConstants();
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                Enum opt = arr$[i$];
                options.add(String.valueOf(opt));
            }

            cache.put(enumClass, options);
        }

        return ((Set)cache.get(enumClass)).contains(value);
    }

    public static <T extends Enum<T>> T fromString(Class<T> enumClass, String value) {
        if(!enumContains(enumClass, value)) {
            throw new IllegalArgumentException("Wrong value provided to the enum: " + enumClass + " : " + value + "!");
        } else {
            return Enum.valueOf(enumClass, value);
        }
    }

    public static <T extends Enum<T>> T fromOrdinal(Class<T> enumClass, Integer value) {
        Enum[] values = enumClass.getEnumConstants();
        if(value.intValue() <= values.length - 1 && value.intValue() >= 0) {
            return (T) values[value.intValue()];
        } else {
            throw new IllegalArgumentException("Wrong value provided for the enum " + enumClass + " : " + value + "!");
        }
    }

    public static <T extends Enum<T>> Collection<Enum<T>> fromStringCollection(final Class<T> enumClass, Collection<String> values) {
        return CollectionUtils.collect(values, new Transformer() {
            public Object transform(Object o) {
                return EnumUtil.fromString(enumClass, o.toString());
            }
        });
    }

    public static Collection<String> toStringCollection(Collection<Enum> values) {
        return CollectionUtils.collect(values, new Transformer() {
            public Object transform(Object o) {
                return ((Enum)o).name();
            }
        });
    }

    public static <T extends Enum<T>> T random(Class<T> enumClass) {
        int rand = RandomUtil.randomInt(enumClass.getEnumConstants().length);
        return fromOrdinal(enumClass, Integer.valueOf(rand));
    }
}
