package just.alive.server.configuration;

import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by Denis on 24.05.2015.
 */
@Configuration
@ComponentScan("just.alive.server.services")
@Import(PersistenceContext.class)
@PropertySource("classpath:application.properties")
public class ApplicationContextConfiguration {
    @Bean
    PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
