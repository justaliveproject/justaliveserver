package just.alive.server.dto.serialization;

import just.alive.server.dto.IAbstractDto;

/**
 * Created by Denis on 24.05.2015.
 */
public interface SerializePostProcessor {
    <ST, DT extends IAbstractDto> void process(final ST result, final DT sourceObject);
}