package just.alive.server.dto.serialization;

import just.alive.server.dto.IAbstractDto;

import java.util.Map;

/**
 * Created by Denis on 24.05.2015.
 */
public interface DtoSerializer<ST> {

    void registerProcessor(SerializeProcessor processor);

    void registerPostProcessor(SerializePostProcessor processor);

    void registerPreProcessor(SerializePreProcessor processor);

    @SuppressWarnings("unchecked")
    <DT extends IAbstractDto> DT deserialize(Class<DT> clazz, ST object);

    @SuppressWarnings("unchecked")
    <DT extends IAbstractDto> DT deserialize(DT instance, ST object);

    @SuppressWarnings("unchecked")
    <DT extends IAbstractDto> ST serialize(DT instance);

    <DT extends IAbstractDto> Map<String, String[]> getFieldsOptions(Class<DT> clazz);

}