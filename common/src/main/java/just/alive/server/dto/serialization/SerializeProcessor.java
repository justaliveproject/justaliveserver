package just.alive.server.dto.serialization;

/**
 * Created by Denis on 24.05.2015.
 */
public interface SerializeProcessor extends SerializePreProcessor, SerializePostProcessor {
}

