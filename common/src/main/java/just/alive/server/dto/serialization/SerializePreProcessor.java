package just.alive.server.dto.serialization;

/**
 * Created by Denis on 24.05.2015.
 */
public interface SerializePreProcessor {
    <DT> void process(final DT sourceObject);
}
