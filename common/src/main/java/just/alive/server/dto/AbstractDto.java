package just.alive.server.dto;

import com.smartfoxserver.v2.entities.data.ISFSObject;

import static just.alive.server.utils.serialization.SFSObjectUtil.deserialize;
import static just.alive.server.utils.serialization.SFSObjectUtil.serialize;

/**
 * Created by Denis on 24.05.2015.
 */
public class AbstractDto implements IAbstractDto {

    public ISFSObject toSFSObject() {
        return serialize(this);
    }

    public void updateFromSFSObject(ISFSObject obj) {
        deserialize(this, obj);
    }

    public String toJson() {
        return toSFSObject().toJson();
    }
}
